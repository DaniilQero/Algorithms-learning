﻿using System;
using System.Numerics;
using System.Text;

namespace Algorithms_learning
{
    public class IsPrimeTest : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Тест числа на простоту малой теоремой Ферма. Какое число проверяем?");
            var valueForTest = IntInput(1);
            WriteWithEmptyLine("Проверяем за 100 тестов.");
            var random = new Random();
            var isPrime = true;

            var sb = new StringBuilder();
            for (int test = 0; test < 100; test++)
            {
                var r = random.Next(1, valueForTest);
                sb.Append($"Берём случайное {r}.");
                if (BigInteger.ModPow(r, valueForTest - 1, valueForTest) == 1)
                {
                    sb.Append($" {r.ToString()} в степени {(valueForTest - 1).ToString()} по модулю {valueForTest.ToString()} == 1.\n");
                    continue;
                }

                sb.Append($" {r.ToString()} - свидетель Ферма, число не простое. Произведено {(test + 1).ToString()} тестов.");
                isPrime = false;
                break;
            }
            
            WriteWithEmptyLine(sb.ToString());

            if (isPrime)
            {
                WriteLine($"Число {valueForTest.ToString()}, вероятно, простое.");
            }
            
            End();
        }
    }
}