﻿namespace Algorithms_learning
{
    public class GCD : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Поиск НОД - наибольшего общего делителя, у двух указанных чисел.");
            WriteWithEmptyLine("Первое число:");
            var number1 = IntInput(0);
            WriteWithEmptyLine("Второе число:");
            var number2 = IntInput(0);
            WriteWithEmptyLine($"Ищем, пока второе число не станет равно 0.");
            while (number2 != 0)
            {
                var remainder = number1 % number2;
                WriteLine($"Остаток от {number1.ToString()} по {number2.ToString()} = {remainder.ToString()}.");
                number1 = number2;
                number2 = remainder;
            }
            
            WriteWithEmptyLine($"НОД = {number1.ToString()}.");
            End();
        }
    }
}