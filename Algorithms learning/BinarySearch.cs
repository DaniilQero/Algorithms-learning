﻿using System.Linq;
using System.Text;

namespace Algorithms_learning
{
    public class BinarySearch : Learn
    {
        private int[] array;

        protected override void OnStart()
        { 
            WriteLine("Бинарный поиск. Поиск числа в отсортированном массиве.");
            PrepareArray();
            Search(GetTarget());
            End();
        }

        private void PrepareArray()
        {
            while (true)
            {
                WriteWithEmptyLine("Начало диапазона:");
                var arrayStart = IntInput();
                WriteWithEmptyLine("Конец диапазона (включительно):");
                var arrayEnd = IntInput(arrayStart);

                array = Enumerable.Range(arrayStart, arrayEnd - arrayStart + 1).ToArray();
                WriteWithEmptyLine("Создан массив:");

                var sb = new StringBuilder();
                foreach (var i in array)
                {
                    sb.Append($"{i.ToString()} ");
                }

                WriteLine(sb.ToString());

                return;
            }
        }

        private int GetTarget()
        {
            while (true)
            {
                WriteWithEmptyLine("Какое ищем число?");
                var target = IntInput();
                if (target >= array[0] && target <= array[^1]) return target;
                WriteLine($"Число {target.ToString()} за рамками массива!");
            }
        }

        private void Search(int target)
        {
            WriteWithEmptyLine($"Начинаем искать {target.ToString()}.");

            var startPosition = 0;
            var endPosition = array.Length - 1;
            var steps = 0;

            while (startPosition <= endPosition)
            {
                steps++;
                
                var checkPosition = (startPosition + endPosition) / 2;
                var checkValue = array[checkPosition];
                
                if (target == checkValue)
                {
                    WriteLine($"Найдено на позиции {checkPosition.ToString()} за {steps.ToString()} шагов.");
                    return;
                }
                
                WriteLine($"Не найдено на позиции {checkPosition.ToString()}.");

                if (target < checkValue)
                {
                    WriteLine($"{target.ToString()} < {checkValue.ToString()}, ищем в предыдущей половине.");
                    endPosition = checkPosition - 1;
                }
                else
                {
                    WriteLine($"{target.ToString()} > {checkValue.ToString()}, ищем в следующей половине.");
                    startPosition = checkPosition + 1;
                }
            }
        }
    }
}