﻿using System.Text;

namespace Algorithms_learning
{
    public class Eratosfen : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Алгоритм находит все простые числа до указанного числа N");
            WriteWithEmptyLine("До какого числа выполняем поиск?");
            var maxNumber = IntInput(2);
            WriteWithEmptyLine("Создаём массив bool, чтобы маркировать числа.");
            var primesNumbers = new bool[maxNumber];
            WriteWithEmptyLine($"Изначально допустим, все числа между 2 и {maxNumber.ToString()} - простые.");
            for (int i = 2; i < maxNumber; i++)
            {
                primesNumbers[i] = true;
            }
            
            WriteWithEmptyLine("Проверяем числа.");
            for (int i = 2; i < maxNumber; i++)
            {
                if (primesNumbers[i])
                {
                    WriteWithEmptyLine($"{i.ToString()} - простое.");

                    var sqr = i * i;
                    if (sqr < maxNumber)
                    {
                        WriteLine("Удаляем все кратные ему из списка простых.");
                    }
                    else
                    {
                        WriteLine("Оставшиеся числа - простые.");
                        break;
                    }
                    
                    for (int j = sqr; j < maxNumber; j += i)
                    {
                        primesNumbers[j] = false;
                    }
                }
            }
            
            var sb = new StringBuilder();
            for (int i = 0; i < maxNumber; i++)
            {
                if (primesNumbers[i])
                {
                    sb.Append($" {i.ToString()}");
                }
            }
            
            WriteWithEmptyLine($"Искомые числа:{sb}");

            End();
        }
    }
}