﻿using System;

namespace Algorithms_learning
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Clear();
            Write("1 - базовые алгоритмы");
            Write("2 - сортировка");
            Write("3 - поиск");

            var actions = new Action[]
            {
                BaseLearns, SortLearns, SearchLearns, delegate { Main(null); }, 
            };
            
            Menu(actions).Invoke();
        }

        private static void BaseLearns()
        {
            Clear();
            Write("1 - возведение в степень.");
            Write("2 - НОД");
            Write("3 - факторизация");
            Write("4 - Решето Эратосфена");
            Write("5 - Проверка на простоту теоремой Ферма");
            Write("0 - назад.");

            var actions = new Action[]
            {
                delegate { StartLearn(new RaiseToPower()); },
                delegate { StartLearn(new GCD()); },
                delegate { StartLearn(new IntegerFactorization()); },
                delegate { StartLearn(new Eratosfen()); },
                delegate { StartLearn(new IsPrimeTest()); },
                delegate { Main(null); }
            };
            
            Menu(actions).Invoke();
        }

        private static void SortLearns()
        {
            Clear();
            Write("1 - Сортировка вставкой");
            Write("2 - Сортировка выбором");
            Write("3 - Пузырьковая сортировка");
            Write("4 - Пирамидальная сортировка");
            Write("5 - Быстрая сортировка");
            Write("0 - назад.");

            var actions = new Action[]
            {
                delegate { StartLearn(new InsertionSort()); },
                delegate { StartLearn(new SelectionSort()); },
                delegate { StartLearn(new Bubblesort()); },
                delegate { StartLearn(new HeapSort()); }, 
                delegate { StartLearn(new QuickSort()); }, 
                delegate { Main(null); }
            };
            
            Menu(actions).Invoke();
        }

        private static void SearchLearns()
        {
            Console.Clear();
            Console.WriteLine("1 - бинарный поиск.");
            Console.WriteLine("0 - назад.");

            var actions = new Action[]
            {
                delegate { StartLearn(new BinarySearch()); },
                delegate { Main(null); }
            };
            
            Menu(actions).Invoke();
        }

        private static void StartLearn(Learn learn)
        {
            learn.Start();
        }
        
        private static void Clear()
        {
            Console.Clear();
        }

        private static void Write(string text)
        {
            Console.WriteLine(text);
        }

        private static Action Menu(Action[] actions)
        {
            while (true)
            {
                var input = Console.ReadKey(true);
                if (!char.IsDigit(input.KeyChar)) continue;
                if (!int.TryParse(input.KeyChar.ToString(), out var index)) continue;
                if (index >= actions.Length) continue;
                if (index < 1) index = actions.Length;

                return actions[index-1];
            }
        }
    }
}