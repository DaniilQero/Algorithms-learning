﻿using System.Text;

namespace Algorithms_learning
{
    public class QuickSort : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Быстрая сортировка. Введите произвольный массив через запятую:");
            var inputArray = IntArrayInput();

            Sort(inputArray, 0, inputArray.Length - 1);
            
            WriteWithEmptyLine("Результат:");
            var sb = new StringBuilder();
            foreach (var i in inputArray)
            {
                sb.Append($"{i.ToString()} ");
            }
            WriteLine(sb.ToString());
            
            End();
        }
        
        private void Sort(int[] array, int startIndex, int endIndex)
        {
            if (startIndex >= endIndex) return;
            
            var middleIndex = startIndex;
            var middleValue = array[middleIndex];

            for (int i = startIndex + 1; i <= endIndex; i++)
            {
                var targetElement = array[i];
                if (targetElement < middleValue)
                {
                    for (int j = i; j > middleIndex; j--)
                    {
                        (array[j - 1], array[j]) = (array[j], array[j - 1]);
                    }

                    middleIndex++;
                }
            }

            Sort(array, startIndex, middleIndex - 1);
            Sort(array, middleIndex + 1, endIndex);
        }
    }
}