﻿using System;

namespace Algorithms_learning
{
    public abstract class Learn
    {
        public void Start()
        {
            Console.Clear();
            OnStart();
        }

        protected abstract void OnStart();

        protected void End()
        {
            WriteWithEmptyLine("1 - повторить, 2 - вернуться в меню.");
            var input = Console.ReadKey(true).KeyChar;
            
            switch (input)
            {
                case '1':
                    Start();
                    break;
                case '2':
                    Program.Main(null);
                    break;
            }
        }

        protected void Write(string input)
        {
            Console.Write(input);
        }

        protected void WriteLine(string input)
        {
            Console.WriteLine(input);
        }

        protected void WriteWithEmptyLine(string input)
        {
            Console.WriteLine(String.Empty);
            Console.WriteLine(input);
        }
        
        protected int IntInput()
        {
            while (true)
            {
                try
                {
                    var s = Console.ReadLine();
                    return int.Parse(s);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Неверный формат: {e}");
                }
            }
        }

        protected int IntInput(int min)
        {
            while (true)
            {
                try
                {
                    var s = Console.ReadLine();
                    var input = int.Parse(s);
                    if (input >= min) return input;
                    Console.WriteLine($"Должно быть от {min.ToString()}!");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Неверный формат: {e}");
                }
            }
        }

        protected int[] IntArrayInput()
        {
            while (true)
            {
                try
                {
                    var s = Console.ReadLine();
                    var a = s.Split(',');
                    var array = new int[a.Length];

                    for (int i = 0; i < a.Length; i++)
                    {
                        array[i] = int.Parse(a[i]);
                    }

                    return array;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Неверный формат: {e}");
                }
            }
        }
    }
}