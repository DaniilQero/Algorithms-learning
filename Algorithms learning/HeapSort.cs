﻿using System;
using System.Text;

namespace Algorithms_learning
{
    public class HeapSort : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Пирамидальная сортировка. Введите произвольный массив через запятую:");
            var inputArray = IntArrayInput();
            
            Sort(inputArray);
            
            WriteWithEmptyLine("Результат:");
            var sb = new StringBuilder();
            foreach (var i in inputArray)
            {
                sb.Append($"{i.ToString()} ");
            }
            WriteLine(sb.ToString());
            
            End();
        }

        private void Sort(int[] array)
        {
            MakeHeap(array);
            SortLargestInHeap(array, array.Length);
        }

        private void MakeHeap(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                var index = i;

                while (index != 0)
                {
                    var parent = (index - 1) / 2;
                    if (array[index] <= array[parent]) break;
                    (array[index], array[parent]) = (array[parent], array[index]);
                    index = parent;
                }
            }
        }

        private void SortLargestInHeap(int[] heap, int heapLenght)
        {
            if (heapLenght == 0) return;
            (heap[0], heap[heapLenght - 1]) = (heap[heapLenght - 1], heap[0]);
            heapLenght--;
            
            var index = 0;
            while (true)
            {
                var child1 = 2 * index + 1;
                var child2 = 2 * index + 2;

                if (child1 >= heapLenght)
                {
                    child1 = index;
                }

                if (child2 >= heapLenght)
                {
                    child2 = index;
                }
                
                if (heap[index] >= heap[child1] && heap[index] >= heap[child2]) break;

                var targetChild = heap[child1] > heap[child2] ? child1 : child2;
                (heap[index], heap[targetChild]) = (heap[targetChild], heap[index]);
                index = targetChild;
            }
            
            SortLargestInHeap(heap, heapLenght);
        }
    }
}