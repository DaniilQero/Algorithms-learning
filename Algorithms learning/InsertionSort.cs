﻿using System.Text;

namespace Algorithms_learning
{
    public class InsertionSort : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Сортировка вставкой. Введите произвольный массив через запятую:");
            var inputArray = IntArrayInput();
            
            Sort(inputArray);
            
            WriteLine("Результат:");
            var sb = new StringBuilder();
            foreach (var i in inputArray)
            {
                sb.Append($"{i.ToString()} ");
            }
            WriteLine(sb.ToString());
            
            End();
        }

        private void Sort(int[] array)
        {
            if (array.Length < 2)
            {
                return;
            }
            
            int valCache;
            int posCache;

            var sb = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                sb.Append($"Проверяем число {array[i].ToString()} на позиции {i.ToString()}.");
                valCache = array[i];
                posCache = i;

                while (posCache > 0 && array[posCache-1] > valCache)
                {
                    sb.Append($" Меняем {array[posCache].ToString()} и {array[posCache - 1].ToString()} местами.");
                    Swap(array, posCache, posCache-1);
                    posCache--;
                }

                sb.Append("\n");
            }
            
            WriteWithEmptyLine(sb.ToString());
        }
        
        private void Swap(int[] array, int i, int j)
        {
            (array[i], array[j]) = (array[j], array[i]);
        }
    }
}