﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Algorithms_learning
{
    public class IntegerFactorization : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Разложение числа на простые множители. Что раскладываем?");
            var target = IntInput(1);
            var targetCash = target;
            var factors = new List<int>();
            
            WriteWithEmptyLine("Проверяем делимость на 2.");
            while (target % 2 == 0)
            {
                WriteLine($"{target.ToString()} делится на 2 без остатка, добавляем 2, а само число делим.");
                factors.Add(2);
                target /= 2;
            }
            
            WriteWithEmptyLine($"Дальше работаем с {target.ToString()}.");

            var i = 3;
            var maxFactor = Math.Sqrt(target);
            
            WriteLine($"Начинаем с 3, 2 уже обработали. Верхняя граница: {maxFactor.ToString(CultureInfo.InvariantCulture)}, это квадратный корень из {target.ToString()}.");

            while (i < maxFactor)
            {
                WriteWithEmptyLine($"Проверяем делимость на {i.ToString()}.");
                while (target % i == 0)
                {
                    WriteLine($"{target.ToString()} делится на {i.ToString()} без остатка, добавляем {i.ToString()}, а само число делим.");
                    factors.Add(i);
                    target /= i;
                    maxFactor = Math.Sqrt(target);
                    WriteLine($"Новая верхняя граница: {maxFactor.ToString(CultureInfo.InvariantCulture)}");
                }

                if (i + 2 <= maxFactor)
                {
                    WriteLine($"Проверяем следующий нечётный множитель.");
                }
                else
                {
                    WriteLine($"Достигли верхней границы возможного множителя.");
                }
                
                i += 2;
            }

            if (target > 0)
            {
                WriteWithEmptyLine($"От числа осталось {target.ToString()}, это тоже множитель.");
                factors.Add(target);
            }

            WriteWithEmptyLine($"Простые множители для {targetCash.ToString()}:");
            var sb = new StringBuilder();
            for (var j = 0; j < factors.Count; j++)
            {
                var factor = factors[j];
                sb.Append($" {factor.ToString()}");
            }
            
            WriteLine(sb.ToString());
            
            End();
        }
    }
}