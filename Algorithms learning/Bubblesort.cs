﻿using System;
using System.Text;

namespace Algorithms_learning
{
    public class Bubblesort : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Пузырьковая сортировка. Введите произвольный массив через запятую:");
            var inputArray = IntArrayInput();
            
            Sort(inputArray);
            
            WriteWithEmptyLine("Результат:");
            var sb = new StringBuilder();
            foreach (var i in inputArray)
            {
                sb.Append($"{i.ToString()} ");
            }
            WriteLine(sb.ToString());
            
            End();
        }

        private void Sort(int[] array)
        {
            if (array.Length < 2)
            {
                return;
            }
            
            var notSorted = true;
            var startPos = 0;
            var end = array.Length - 1;
            var step = 1;
            var count = 0;
            var lastIndex = 0;
            
            Func<int, int, bool> comparison = DirectComparison;

            while (notSorted)
            {
                count++;
                notSorted = false;
                
                WriteLine($"Проход #{count.ToString()}");
                WriteLine($"Начинаем со значения {startPos.ToString()}");
                WriteLine($"Идём с шагом {step}");
                WriteLine($"До {end}");

                for (int i = startPos; comparison(i, end); i += step)
                {
                    var current = array[i];
                    var next = array[i + step];
                    
                    if (comparison(next, current))
                    {
                        WriteLine($"Меняем местами {current.ToString()} и {next.ToString()}");
                        (array[i], array[i+step]) = (array[i+step], array[i]);
                        lastIndex = i;
                        notSorted = true;
                    }
                }

                end = lastIndex;
                (startPos, end) = (end, startPos);
                step *= -1;
                comparison = step == 1 ? DirectComparison : ReverseComparison;
            }

            bool DirectComparison(int value1, int value2)
            {
                return value1 < value2;
            }

            bool ReverseComparison(int value1, int value2)
            {
                return value1 > value2;
            }
        }
    }
}