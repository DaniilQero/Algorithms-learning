﻿using System.Text;

namespace Algorithms_learning
{
    public class SelectionSort : Learn
    {
        protected override void OnStart()
        {            
            WriteLine("Сортировка вставкой. Введите произвольный массив через запятую:");
            var inputArray = IntArrayInput();
            
            Sort(inputArray);
            
            WriteWithEmptyLine("Результат:");
            var sb = new StringBuilder();
            foreach (var i in inputArray)
            {
                sb.Append($"{i.ToString()} ");
            }
            WriteLine(sb.ToString());
            
            End();
        }

        private void Sort(int[] array)
        {
            if (array.Length < 2)
            {
                return;
            }
            
            int cache;
            
            WriteWithEmptyLine($"Для каждого числа проверяем все последующие числа, ищем то, что меньше.");
            for (int i = 0; i < array.Length; i++)
            {
                cache = i;
                WriteWithEmptyLine($"Число {array[i].ToString()}.");
                for (int j = i; j < array.Length; j++)
                {
                    if (array[j] < array[cache])
                    {
                        WriteLine($"{array[j].ToString()} на позиции {j.ToString()} меньше, заносим в кэш.");
                        cache = j;
                    }
                }
                
                if (i != cache)
                {
                    WriteLine($"Меняем местами {array[i].ToString()} на позиции {i.ToString()} и {array[cache].ToString()} на позиции {cache.ToString()}.");
                    Swap(array, i, cache);
                }
                else
                {
                    WriteLine($"Меньшего числа не найдено.");
                }
            }
            
            WriteLine($"Проверили все числа.");
        }
        
        private void Swap(int[] array, int i, int j)
        {
            (array[i], array[j]) = (array[j], array[i]);
        }
    }
}