﻿namespace Algorithms_learning
{
    public class RaiseToPower : Learn
    {
        protected override void OnStart()
        {
            WriteLine("Возведение в степень. Какое число возводим?");
            var numberInput = IntInput();
            WriteWithEmptyLine("В какую степень?");
            var targetPower = IntInput(2);

            var power = 1;
            long value1 = numberInput;
            var value2 = numberInput;

            WriteWithEmptyLine($"Ищем степень, кратную двум, но не больше целевой степени.");
            while (power * 2 <= targetPower)
            {
                power *= 2;
                value1 *= value1;
                WriteLine($"{numberInput.ToString()} в степени {power.ToString()} = {value1.ToString()}");
            }
            
            var delta = targetPower - power;
            if (delta > 0)
            {
                WriteWithEmptyLine($"Разница в текущей и целевой степени: {delta}");
                
                for (int i = 1; i < delta; i++)
                {
                    value2 *= numberInput;
                    WriteLine($"{numberInput.ToString()} в степени {(i+1).ToString()} = {value2.ToString()}");
                }
                
                WriteLine($"Перемножаем {value1.ToString()} и {value2.ToString()}");
                value1 *= value2;
            }
            
            WriteWithEmptyLine($"Результат: {value1.ToString()}");
            
            End();
        }
    }
}